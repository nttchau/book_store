import { engine } from "express-handlebars";
import moment from "moment";
import numeral from "numeral";
import hbs_sections from "express-handlebars-sections";

export default function (app) {
  app.engine(
    "hbs",
    engine({
      //   defaultLayout: "main.hbs",
      partialsDir: "views/partials/",
      extname: ".hbs",
      helpers: {
        format_price(val) {
          return numeral(val).format("0,0");
        },
        section: hbs_sections(),

        isAdmin(type) {
          if (type === "admin") return true;
          return false;
        },
        isStaff(type) {
          if (type === "staff") return true;
          return false;
        },
        isUser(type) {
          if (type === "user") return true;
          return false;
        },
        notUser(type) {
          if (type === "user") return false;
          return true;
        },
        displayRole(type) {
          switch (type) {
            case "user":
              return "Khách hàng";
            case "admin":
              return "Quản trị viên";
            case "staff":
              return "Nhân viên";
            default:
              return "";
          }
        },
        format_name(name) {
          if (name.length > 15) {
            return name.substring(0, 15) + "...";
          } else return name;
        },
        increase(value) {
          value++;
        },
        checkAuth(authID, id) {
          if (authID === id) return false;
          return true;
        },
        displayStatus(type) {
          switch (type) {
            case 0:
              return "Chưa thanh toán";
            case 1:
              return "Đã thanh toán";
            default:
              return "";
          }
        },
        formatTime(date, format) {
          var mmt = moment(date);
          return mmt.format(format);
        },
        isPaid(type) {
          if (type == 0) return false;
          return true;
        },
        formatDate(date) {
          return moment(date).format("DD/MM/YYYY hh:mm ");
        },
      },
    })
  );
  app.set("view engine", "hbs");
  app.set("views", "./views");
}
