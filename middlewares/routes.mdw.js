import accountRouter from "../routes/account.route.js";
import bookManagementRouter from "../routes/book-management.route.js";
import categoryManagementRouter from "../routes/category-management.route.js";
import publisherManagementRouter from "../routes/publisher-management.route.js";
import userManagementRouter from "../routes/user-management.route.js";
import bookRouter from "../routes/book.route.js";
import profileRouter from "../routes/account-profile.route.js";
import homeRouter from "../routes/home.route.js";
import cartRoute from "../routes/cart.route.js";
import statisticRouter from "../routes/statistic.route.js";
import orderRouter from "../routes/order-management.route.js";
import paymentRouter from "../routes/payment.route.js";

import { dirname } from "path";
import { fileURLToPath } from "url";

const __dirname = dirname(fileURLToPath(import.meta.url));
export default function (app) {
  // app.get("/err", function (req, res) {
  //   throw new Error("Error!");
  // });

  app.use("/books", bookRouter);
  app.use("/", homeRouter);
  app.use("/", accountRouter);
  app.use("/", profileRouter);
  app.use("/cart", cartRoute);
  app.use("/management/", bookManagementRouter);
  app.use("/management/book", bookManagementRouter);
  app.use("/management/category", categoryManagementRouter);
  app.use("/management/publisher", publisherManagementRouter);
  app.use("/management/user", userManagementRouter);
  app.use("/management/statistic", statisticRouter);
  app.use("/management/order", orderRouter);
  app.use("/", paymentRouter);
  //   app.use(function (req, res, next) {
  //     res.render("404", { layout: false });
  //   });

  //   app.use(function (err, req, res, next) {
  //     console.error(err.stack);
  //     res.render("500", { layout: false });
  //   });
}
