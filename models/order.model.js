import db from "../utils/db.js";

export default{
    async add(entity){
        return await db('dondathang').insert(entity);
    },
    async getLastId(){
        const id = await db('dondathang').max('id');
      return id[0]['max(`id`)'];
    },
    async getTransport(id){
        const list = await db('dondathang').select('PTGiaoHang').where("id",id);
        return list[0]["PTGiaoHang"];
    },
    async findUserOrder(user){
        if (user == null)
        return null;
        const list = await db('dondathang').where('NguoiDat',user);
        return list;
    },
    async getTotal(id){
        const list = await db('dondathang').select('TongTien').where("id",id);
        return list[0]["TongTien"];
    },
}