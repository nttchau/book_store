import db from "../utils/db.js";
export default {
  // delInProducts(prodID) {
  //   return db("products").where("prodID", prodID).del();
  // },
  // delInRating(prodID) {
  //   return db("rating").where("prodID", prodID).del();
  // },
  // delInDeclined(prodID) {
  //   return db("declined").where("prodID", prodID).del();
  // },
  // delInFavorite(prodID) {
  //   return db("favoriteproducts").where("prodID", prodID).del();
  // },
  // delInPaticipate(prodID) {
  //   return db("participate").where("prodID", prodID).del();
  // },
  // delInProddes(prodID) {
  //   return db("proddes").where("prodID", prodID).del();
  // },

  // async findProductByCatID(catID) {
  //   const sql = `select p.*, pt.typeName, c.catID from products p  left join producttype pt on p.prodType = pt.typeID
  //         left join categories c on pt.category = c.catID
  //         where c.catID = ${catID};`;
  //   const raw = await db.raw(sql);
  //   // console.log(raw[0]);
  //   return raw[0];
  // },
  // async findProductByTypeID(typeID) {
  //   const sql = ` select * from products p left join producttype pt on p.prodType = pt.typeID where pt.typeID = ${typeID};`;
  //   const raw = await db.raw(sql);
  //   // console.log(raw[0]);
  //   return raw[0];
  // },
  async findAllCat() {
    const total = await db("theloai").where("TinhTrang", 1);
    return total;
  },

  async countAllCat() {
    const total = await db("theloai").where("TinhTrang", 1);
    return total.length;
  },

  async findByCatId(catId) {
    const category = await db("theloai")
      .where("id", catId)
      .where("TinhTrang", 1);
    // console.log(category);
    return category[0];
  },

  add(TenTheLoai) {
    return db("theloai").insert({ TenTheLoai });
  },

  async update(id, TenTheLoai) {
    await db("theloai").where("id", id).update("TenTheLoai", TenTheLoai);
  },

  async delete(id) {
    await db("theloai").where("id", id).update("TinhTrang", 0);
  },

  async findProductByCatID(catID) {
    const sql = `select * from sach s left join theloai t on s.TheLoai = t.id
    where s.TheLoai = ${catID} and t.TinhTrang = 1;`;
    const raw = await db.raw(sql);
    // console.log(raw[0]);
    return raw[0];
  },
};
