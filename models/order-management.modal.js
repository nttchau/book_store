import db from "../utils/db.js";

export default {
  async findAll() {
    const sql = `select d.id, t.HoTen, d.ThoiDiemDatHang, d.TongTien, gh.Ten, d.TinhTrang from dondathang d left join taikhoan t on d.NguoiDat = t.id left join loaigiaohang gh on d.PTGiaoHang = gh.id`;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findInYear(year) {
    const sql = `select d.id, t.HoTen, d.ThoiDiemDatHang, d.TongTien, gh.Ten, d.TinhTrang from dondathang d left join taikhoan t on d.NguoiDat = t.id left join loaigiaohang gh on d.PTGiaoHang = gh.id
    where year(d.ThoiDiemDatHang) = ${year}`;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findInMonth(month, year) {
    const sql = `select d.id, t.HoTen, d.ThoiDiemDatHang, d.TongTien, gh.Ten, d.TinhTrang from dondathang d left join taikhoan t on d.NguoiDat = t.id left join loaigiaohang gh on d.PTGiaoHang = gh.id
    where month(d.ThoiDiemDatHang) = ${month} and year(d.ThoiDiemDatHang) = ${year}`;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async update(id) {
    await db("dondathang").where("id", id).update("TinhTrang", 1);
  },
};
