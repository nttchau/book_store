import db from "../utils/db.js";

export default {
  add(entity) {
    return db("sach").insert(entity);
  },
  async delete(id) {
    await db("sach").where("id", id).del();
  },
  async disableBook(id) {
    await db("sach").where("id", id).update("HieuLuc", "0");
  },
  async update(id, book) {
    await db("sach").where("id", id).update(book);
  },
async getStock(id)  
{
  const list = await db("sach").select("SoLuongTon").where("id",id);
  return list[0]["SoLuongTon"];
},
async updateStock(id, amount){
  await db("sach").where("id", id).update("SoLuongTon", amount);
},
  async findAll() {
    const sql = `select p.*, t.TenTheLoai as TenTheLoai, a.TenTacGia as TenTacGia, u.HoTen as Editor,n.TenNXB as TenNXB
                        from sach p left join theloai t on p.TheLoai = t.id
                        left join nhaxuatban n on p.NXB= n.id
                        left join taikhoan u on p.LastEditor = u.id
                        left join tacgia a on p.TacGia = a.id
                        where p.HieuLuc = "1"`;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findAllPage(limit, offset) {
    const sql =
      `select p.*, t.TenTheLoai as TenTheLoai, a.TenTacGia as TenTacGia, u.HoTen as Editor,n.TenNXB as TenNXB
                        from sach p left join theloai t on p.TheLoai = t.id
                        left join nhaxuatban n on p.NXB= n.id
                        left join taikhoan u on p.LastEditor = u.id
                        left join tacgia a on p.TacGia = a.id
                        where p.HieuLuc = "1"
                        limit ` +
      limit +
      ` offset ` +
      offset;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async countAll() {
    const list = await db("sach").count({ amount: "id" }).where("HieuLuc", 1);
    return list[0].amount;
  },
  async countByCat(id) {
    const list = await db("sach")
      .count({ amount: "id" })
      .where({ HieuLuc: 1, TheLoai: id });
    return list[0].amount;
  },
  async findPageAll(limit, offset) {
    const sql =
      `select p.*, t.TenTheLoai as TenTheLoai
                        from sach p left join theloai t
                        on p.TheLoai = t.id
                        where p.HieuLuc = "1" and t.TinhTrang = "1"
                        order by p.NamXB desc
                        limit ` +
      limit +
      ` offset ` +
      offset;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findPageAllPrice(limit, offset) {
    const sql =
      `select p.*, t.TenTheLoai as TenTheLoai
                        from sach p left join theloai t
                        on p.TheLoai = t.id
                        where p.HieuLuc = "1" and t.TinhTrang = "1"
                        order by p.GiaSach asc
                        limit ` +
      limit +
      ` offset ` +
      offset;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findByCat(id) {
    const sql =
      `select p.*, t.TenTheLoai as TenTheLoai
                        from sach p left join theloai t
                        on p.TheLoai = t.id
                        where p.HieuLuc = "1" and t.id =` +
      id +
      ` and t.TinhTrang = "1"
                        order by p.NamXB desc`;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findPageByCat(id, limit, offset) {
    const sql =
      `select p.*, t.TenTheLoai as TenTheLoai
                        from sach p left join theloai t
                        on p.TheLoai = t.id
                        where p.HieuLuc = "1" and t.id =` +
      id +
      ` and t.TinhTrang = "1"
                        order by p.NamXB desc
                        limit ` +
      limit +
      ` offset ` +
      offset;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findPageByCatPrice(id, limit, offset) {
    const sql =
      `select p.*, t.TenTheLoai as TenTheLoai
                        from sach p left join theloai t
                        on p.TheLoai = t.id
                        where p.HieuLuc = "1" and t.id =` +
      id +
      ` and t.TinhTrang = "1"
                        order by p.GiaSach asc
                        limit ` +
      limit +
      ` offset ` +
      offset;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findById(id) {
    const sql =
      `select p.*, t.TenTheLoai as TenTheLoai, n.TenNXB,a.TenTacGia, u.HoTen
        from sach p left join theloai t   on p.TheLoai = t.id
        left join nhaxuatban n on p.NXB = n.id
        left join tacgia a on p.TacGia = a.id
        left join taikhoan u on p.LastEditor = u.id
        where p.HieuLuc = "1"  and p.id =` +
      id +
      ` and t.TinhTrang = "1"
        order by p.NamXB desc`;
    const raw = await db.raw(sql);
    return raw[0][0];
  },
  async searchBooks(keyword, filter, limit, offset) {
    let sql = "";
    if (filter == "0") {
      sql =
        `select p.*, t.TenTheLoai as TenTheLoai
        from sach p left join theloai t
        on p.TheLoai = t.id
        where p.HieuLuc = "1" and t.TinhTrang = "1" and match (p.TenSach) against ("` +
        keyword +
        `")
        order by p.NamXB desc
        limit ` +
        limit +
        ` offset ` +
        offset;
    } else {
      sql =
        `select p.*, t.TenTheLoai as TenTheLoai
            from sach p left join theloai t
            on p.TheLoai = t.id
            where p.HieuLuc = "1" and t.TinhTrang = "1" and match (p.TenSach) against ("` +
        keyword +
        `") and p.TheLoai=` +
        filter +
        `
            order by p.NamXB desc
            limit ` +
        limit +
        ` offset ` +
        offset;
    }
    const raw = await db.raw(sql);
    return raw[0];
  },
  async searchBooksPrice(keyword, filter, limit, offset) {
    let sql = "";
    if (filter == "0") {
      sql =
        `select p.*, t.TenTheLoai as TenTheLoai
        from sach p left join theloai t
        on p.TheLoai = t.id
        where p.HieuLuc = "1" and t.TinhTrang = "1" and match (p.TenSach) against ("` +
        keyword +
        `")
        order by p.GiaSach asc
        limit ` +
        limit +
        ` offset ` +
        offset;
    } else {
      sql =
        `select p.*, t.TenTheLoai as TenTheLoai
            from sach p left join theloai t
            on p.TheLoai = t.id
            where p.HieuLuc = "1" and t.TinhTrang = "1" and match (p.TenSach) against ("` +
        keyword +
        `") and p.TheLoai=` +
        filter +
        `
            order by p.GiaSach asc
            limit ` +
        limit +
        ` offset ` +
        offset;
    }
    const raw = await db.raw(sql);
    return raw[0];
  },
  async countSearch(keyword, filter) {
    let sql = "";
    if (filter == "0") {
      sql =
        `select count(p.id) as amount
         from sach p left join theloai t
         on p.TheLoai = t.id
         where p.HieuLuc = "1" and t.TinhTrang = "1" and match (p.TenSach) against ("` +
        keyword +
        `")`;
    } else {
      sql =
        `select count(p.id) as amount
             from sach p left join theloai t
             on p.TheLoai = t.id
             where p.HieuLuc = "1" and t.TinhTrang = "1" and match (p.TenSach) against ("` +
        keyword +
        `") and p.TheLoai=` +
        filter;
    }
    const raw = await db.raw(sql);

    return raw[0][0].amount;
  },
  async findPriceById(id){
    const price = await db('sach').select("GiaSach").where("id",id);
    return price[0]['GiaSach'];
  },
  async getLastInsert(){
      const id = await db('sach').max('id');
      return id[0]['max(`id`)'];
  }
};
