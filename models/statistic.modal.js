import db from "../utils/db.js";

export default {
  async findAll() {
    const sql = `select s.id, s.TenSach, s.GiaSach, sum(ct.SoLuong) as SoLuongBan, s.GiaSach*sum(ct.SoLuong) as TongTien, s.SoLuongTon from ct_dondathang ct left join dondathang d on ct.id=d.id left join sach s on ct.SachId = s.id
    group by s.id`;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findInYear(year) {
    const sql = `select s.id, s.TenSach, s.GiaSach, sum(ct.SoLuong) as SoLuongBan, s.GiaSach*sum(ct.SoLuong) as TongTien, s.SoLuongTon, d.ThoiDiemDatHang as ThoiGian from ct_dondathang ct left join dondathang d on ct.id=d.id left join sach s on ct.SachId = s.id
    where year(d.ThoiDiemDatHang) = ${year}
    group by s.id
    `;
    const raw = await db.raw(sql);
    return raw[0];
  },
  async findInMonth(month, year) {
    const sql = `select s.id, s.TenSach, s.GiaSach, sum(ct.SoLuong) as SoLuongBan, s.GiaSach*sum(ct.SoLuong) as TongTien, s.SoLuongTon, d.ThoiDiemDatHang as ThoiGian from ct_dondathang ct left join dondathang d on ct.id=d.id left join sach s on ct.SachId = s.id
    where month(d.ThoiDiemDatHang) = ${month} and year(d.ThoiDiemDatHang) = ${year}
    group by s.id
    `;
    const raw = await db.raw(sql);
    return raw[0];
  },
};
