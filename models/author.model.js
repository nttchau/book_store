import db from "../utils/db.js";
export default {
  async findAll() {
    const total = await db("tacgia");
    return total;
  },

  async findById(id) {
    const category = await db("tacgia").where("id", id);
    // console.log(category);
    return category[0];
  },

  add(TenTacGia) {
    return db("tacgia").insert({ TenTacGia });
  },

 
};
