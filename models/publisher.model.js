import db from "../utils/db.js";
export default {
  async findAll() {
    const total = await db("nhaxuatban");
    return total;
  },

  async findById(id) {
    const category = await db("nhaxuatban").where("id", id);
    // console.log(category);
    return category[0];
  },

  add(TenNXB) {
    return db("nhaxuatban").insert({ TenNXB });
  },

  async update(id, TenNXB) {
    await db("nhaxuatban").where("id", id).update("TenNXB", TenNXB);
  },

  async delete(id) {
    await db("nhaxuatban").where("id", id).del();
  },

  async findProductByPublisherID(id) {
    const sql = `select * from sach s left join nhaxuatban n on s.NXB = n.id where s.NXB = ${id}`;
    const raw = await db.raw(sql);
    // console.log(raw[0]);
    return raw[0];
  },
};
