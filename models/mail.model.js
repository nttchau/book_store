import { fromMail, transporter } from "../utils/transporter.js";
export default {
  //-----------------Hàm cho Mail-------------------
  sendAuctionEmail(recvEmail, subject, text) {
    // console.log(recvEmail + " " + subject + " " + text);

    var mailOptions = {
      from: fromMail,
      to: recvEmail,
      subject: subject,
      text: text,
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
  },
};
