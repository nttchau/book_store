import db from "../utils/db.js";

export default {
  async findByEmail(email) {
    const list = await db("taikhoan").where("email", email);
    if (list.length === 0) return null;
    return list[0];
  },
  add(entity) {
    return db("taikhoan").insert(entity);
  },
  async editUser(uID, entity) {
    await db("taikhoan").where("ID", uID).update(entity);
  },

  async findAll() {
    const total = await db("taikhoan");
    return total;
  },

  async findById(id) {
    const user = await db("taikhoan").where("id", id).where("HoatDong", 1);
    // console.log(user);
    return user[0];
  },

  async delete(id) {
    await db("taikhoan").where("id", id).update("HoatDong", 0);
  },

  async unblock(id) {
    await db("taikhoan").where("id", id).update("HoatDong", 1);
  },

  async findByEmail(email) {
    const list = await db("taikhoan").where("email", email);
    if (list.length === 0) return null;
    return list[0];
  },
  add(entity) {
    return db("taikhoan").insert(entity);
  },
  async editUser(uID, entity) {
    await db("taikhoan").where("ID", uID).update(entity);
    return;
  },
  async updatepassword(id, newPw) {
    await db("taikhoan").where("ID", id).update("pssword", newPw);
    return;
  },
};
