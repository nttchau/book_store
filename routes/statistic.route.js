import express from "express";
import bookModel from "../models/book.model.js";
import categoryModel from "../models/category.model.js";
import auth from "../middlewares/auth.mdw.js";
import multer from "multer";
import publisherModel from "../models/publisher.model.js";
import authorModel from "../models/author.model.js";
import statisticModal from "../models/statistic.modal.js";

const router = express.Router();

router.get("/", auth, async (req, res) => {
  const list = await statisticModal.findAll();
  let total = 0;
  for (let item of list) {
    total += +item.TongTien;
  }

  res.render("management/statistic", {
    layout: "account.hbs",
    list,
    total,
    year: 2022,
  });
});

router.post("/", auth, async (req, res) => {
  const month = req.body.month;
  const year = req.body.year;
  // console.log(month);
  // console.log(year);
  let list = [];
  if (month == 0) {
    list = await statisticModal.findInYear(year);
  } else {
    // console.log(year);
    list = await statisticModal.findInMonth(month, year);
  }

  // console.log(list);
  let total = 0;
  for (let item of list) {
    total += +item.TongTien;
  }

  res.render("management/statistic", {
    layout: "account.hbs",
    list,
    total,
    year,
  });
});

export default router;
