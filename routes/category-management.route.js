import express from "express";
import categoryModel from "../models/category.model.js";
const router = express.Router();

router.get("/", async (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }

  const catList = await categoryModel.findAllCat();

  res.render("management/category", {
    layout: "account.hbs",
    categoryList: catList,
  });
});

router.post("/delete", async (req, res) => {
  const delID = req.body.id;
  await categoryModel.delete(delID);
  res.redirect(req.headers.referer);
});

router.get("/has-products/:catID", async function (req, res) {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  const catName = await categoryModel.findProductByCatID(req.params.catID);
  if (catName.length === 0) {
    return res.json(true);
  }
  res.json(false);
});

router.get("/add", (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  res.render("add/category", { layout: "account.hbs" });
});
router.post("/add", async (req, res) => {
  const catName = req.body.catName;
  await categoryModel.add(catName);

  res.redirect(req.headers.referer);
});

router.get("/edit/:catId", async (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  const catId = req.params.catId || 0;

  const catInfo = await categoryModel.findByCatId(catId);
  res.render("edit/category", {
    layout: "account.hbs",
    catInfo,
  });
});

router.post("/edit/:catId", async (req, res) => {
  const id = req.body.id || 0;
  const TenTheLoai = req.body.TenTheLoai;

  await categoryModel.update(id, TenTheLoai);

  res.redirect(req.headers.referer);
});

export default router;
