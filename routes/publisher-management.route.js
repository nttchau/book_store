import express from "express";
import publisherModel from "../models/publisher.model.js";

const router = express.Router();

router.get("/", async (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  const publisherList = await publisherModel.findAll();

  res.render("management/publisher", { layout: "account.hbs", publisherList });
});
router.get("/add", (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  res.render("add/publisher", { layout: "account.hbs" });
});

router.post("/add", async (req, res) => {
  const pName = req.body.TenNXB;
  await publisherModel.add(pName);

  res.redirect(req.headers.referer);
});

router.get("/edit/:pID", async (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  const pID = req.params.pID || 0;

  const publisherInfo = await publisherModel.findById(pID);
  res.render("edit/publisher", {
    layout: "account.hbs",
    publisherInfo,
  });
});

router.post("/edit/:pID", async (req, res) => {
  const id = req.body.id || 0;

  const TenNXB = req.body.TenNXB;

  await publisherModel.update(id, TenNXB);

  res.redirect(req.headers.referer);
});

router.post("/delete", async (req, res) => {
  const delID = req.body.id;
  await publisherModel.delete(delID);
  res.redirect(req.headers.referer);
});

router.get("/has-products/:pID", async function (req, res) {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  const books = await publisherModel.findProductByPublisherID(req.params.pID);
  if (books.length === 0) {
    return res.json(true);
  }
  res.json(false);
});

export default router;
