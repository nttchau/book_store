import express from "express";
import auth from "../middlewares/auth.mdw.js";
import orderModal from "../models/order-management.modal.js";

const router = express.Router();

router.get("/", auth, async (req, res) => {
  const list = await orderModal.findAll();
  let total = 0;
  for (let item of list) {
    total += +item.TongTien;
  }
  // console.log(list);
  res.render("management/order", {
    layout: "account.hbs",
    list,
    total,
    year: 2022,
  });
});

router.post("/", auth, async (req, res) => {
  const month = req.body.month;
  const year = req.body.year;
  // console.log(month);
  // console.log(year);
  let list = [];
  if (month == 0) {
    list = await orderModal.findInYear(year);
  } else {
    // console.log(year);
    list = await orderModal.findInMonth(month, year);
  }

  // console.log(list);
  let total = 0;
  for (let item of list) {
    total += +item.TongTien;
  }

  res.render("management/order", {
    layout: "account.hbs",
    list,
    total,
    year,
  });
});

router.post("/update", auth, async (req, res) => {
  const id = req.body.id;
  await orderModal.update(id);
  res.redirect(req.headers.referer);
});

export default router;
