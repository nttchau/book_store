import express from "express";
import bookModel from "../models/book.model.js";
import categoryModel from "../models/category.model.js";

const router = express.Router();

router.get("/sortPrice", async (req, res) => {
  // res.sendFile(__dirname + "\\views\\index.html");
  // const list = await bookModel.findAll();
  const limit = 8;
  const page = req.query.page || 1;
  const offset = (page - 1) * limit;

  const total = await bookModel.countAll();
  let nPages = Math.floor(total / limit);
  if (total % limit > 0) {
    nPages++;
  }
  const pageNumbers = [];
  for (let i = 1; i <= nPages; i++) {
    pageNumbers.push({
      value: i,
      isCurrent: +page === i,
    });
  }
  let previous = +page - 1;
  let next = +page + 1;

  const list = await bookModel.findPageAllPrice(limit, offset);

  res.render("home", {
    layout: "main.hbs",
    books: list,
    empty: list.length === 0,
    pageNumbers,
    sortPrice: 1,
    can_go_prev: +page > 1,
    can_go_next: +page < nPages,
    previous,
    next,
  });
});
router.get("/", async (req, res) => {
  // res.sendFile(__dirname + "\\views\\index.html");
  // const list = await bookModel.findAll();
  const limit = 8;
  const page = req.query.page || 1;
  const offset = (page - 1) * limit;

  const total = await bookModel.countAll();
  let nPages = Math.floor(total / limit);
  if (total % limit > 0) {
    nPages++;
  }
  const pageNumbers = [];
  for (let i = 1; i <= nPages; i++) {
    pageNumbers.push({
      value: i,
      isCurrent: +page === i,
    });
  }
  let previous = +page - 1;
  let next = +page + 1;

  const list = await bookModel.findPageAll(limit, offset);

  res.render("home", {
    layout: "main.hbs",
    books: list,
    empty: list.length === 0,
    pageNumbers,
    sortTime: 1,
    can_go_prev: +page > 1,
    can_go_next: +page < nPages,
    previous,
    next,
  });
});



export default router;
