import express from "express";
import bookModel from "../models/book.model.js";
import categoryModel from "../models/category.model.js";
import auth from "../middlewares/auth.mdw.js";
import multer from "multer";
import publisherModel from "../models/publisher.model.js";
import authorModel from "../models/author.model.js";

const router = express.Router();

router.get("/", auth, async (req, res) => {
  const limit = 6;
  const page = req.query.page || 1;
  const offset = (page - 1) * limit;

  const total = await bookModel.countAll();
  let nPages = Math.floor(total / limit);
  if (total % limit > 0) {
    nPages++;
  }
  const pageNumbers = [];
  for (let i = 1; i <= nPages; i++) {
    pageNumbers.push({
      value: i,
      isCurrent: +page === i,
    });
  }

  const list = await bookModel.findAllPage(limit, offset);

  res.render("management/book", {
    layout: "account.hbs",
    books: list,
    pageNumbers,
  });
});

router.get("/add", auth, async (req, res) => {
  const cagtegories = await categoryModel.findAllCat();
  const publisher = await publisherModel.findAll();
  const author = await authorModel.findAll();
  res.render("add/book", {
    layout: "account.hbs",
    cagtegories,
    publisher,
    author,
  });
});
router.post("/add", async (req, res) => {
  const cagtegories = await categoryModel.findAllCat();
  const publisher = await publisherModel.findAll();
  const author = await authorModel.findAll();
  const numb = (await bookModel.countAll()) + 1;

  const filepath = "./public/images";
  // console.log(filepath);

  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, "./public/images");
    },
    filename: function (req, file, cb) {
      const filename = String(numb) + ".jpg";
      cb(null, filename);
    },
  });
  const upload = multer({ storage });
  upload.single("fileUpload")(req, res, async function (err) {
    if (err) {
      // console.log(err);
    } else {
      // console.log(req.body);

      const info = req.body;
      let tmp = info;
      tmp.id = numb;
      tmp.HieuLuc = true;
      tmp.LastEditor = res.locals.authUser.id;
      delete tmp["fileUpload"];
      // console.log(tmp);
      await bookModel.add(tmp);
      const detail = "/";
      const url = detail || "/";
      res.redirect(url);
      return;
    }
  });

  //res.redirect(req.headers.referer);
});
router.get("/edit/:id", async (req, res) => {
  const bookId = req.params.id || 0;

  const book = await bookModel.findById(bookId);

  const cagtegories = await categoryModel.findAllCat();
  const publisher = await publisherModel.findAll();
  const author = await authorModel.findAll();

  res.render("edit/book", {
    layout: "account.hbs",
    book,
    cagtegories,
    publisher,
    author,
  });
});

router.post("/edit/:id", async (req, res) => {
  const bookId = req.params.id || 0;

  const book = await bookModel.findById(bookId);

  const cagtegories = await categoryModel.findAllCat();
  const publisher = await publisherModel.findAll();
  const author = await authorModel.findAll();

  // console.log(req.body);
  const info = req.body;
  let tmp = info;
  tmp.SoLuongTon = book.SoLuongTon;
  tmp.HieuLuc = book.HieuLuc;
  tmp.LastEditor = res.locals.authUser.id;
  await bookModel.update(bookId, tmp);
  const mes = "Cập nhật thành công";

  const after = await bookModel.findById(bookId);
  res.render("edit/book", {
    layout: "account.hbs",
    book: after,
    cagtegories,
    publisher,
    author,
    mes,
  });
  return;
});
router.post("/delete", async (req, res) => {
  const delID = req.body.id;
  // console.log(delID);
  await bookModel.disableBook(delID);
  res.redirect(req.headers.referer);
});
export default router;
