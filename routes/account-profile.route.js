import express from "express";
import bcrypt from "bcrypt";
import auth from "../middlewares/auth.mdw.js";
const router = express.Router();

import tkModel from "../models/taikhoan.model.js";

router.get("/profile", auth, (req, res) => {
  res.render("account/profile", { layout: "account.hbs" });
});

router.get("/password", (req, res) => {
  res.render("account/password", { layout: "account.hbs" });
});

router.get("/profile", auth, (req, res) => {
  res.render("account/profile", { layout: "account.hbs" });
});

router.get("/password", auth, (req, res) => {
  res.render("account/password", { layout: "account.hbs" });
});
router.post("/password", auth, async (req, res) => {
  // console.log(res.locals.authUser);
  // console.log(req.body);
  const id = res.locals.authUser.id;
  const rawPassword = res.locals.authUser.pssword;
  const oldPw = req.body.oldPw;
  const newPw = req.body.newPw;
  const confirm = req.body.confirm;
  const cond = bcrypt.compareSync(oldPw, rawPassword);
  if (!cond) {
    res.render("account/password", {
      layout: "account",
      err: "Mật khẩu không hợp lệ",
    });
    return;
  }
  if (newPw !== confirm) {
    res.render("account/password", {
      layout: "account",
      err: "Mật khẩu nhập lại không hợp lệ",
    });
    return;
  }
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(newPw, salt);
  try {
    await tkModel.updatepassword(id, hash);
  } catch {
    res.render("account/password", {
      layout: "account",
      err: "Thay đổi mật khẩu không thành công. Xin thử lại sau",
    });
    return;
  }
  // req.session.auth = false;
  // req.session.authUser = null;
  // res.locals.auth = false;
  // res.locals.authUser = null;
  // res.redirect("/login");
  res.render("account/password", {
    layout: "account",
    mes: "Cập nhật mật khẩu thành công!",
  });
  return;
});

router.post("/profile", auth, async function (req, res) {
  console.log(req.body);

  const temp = req.body;
  var user = await tkModel.findByEmail(res.locals.authUser.Email);

  if (user !== null) {
    if (
      temp.HoTen === user.HoTen &&
      temp.SDT === user.SDT &&
      temp.DiaChi === user.DiaChi
    ) {
      res.render("account/profile", {
        layout: "account",
      });
      return;
    }
    const id = user.id;

    if (temp.HoTen != user.HoTen) {
      user.HoTen = temp.HoTen;
    }
    if (temp.SDT != user.SDT) {
      user.SDT = temp.SDT;
    }
    if (temp.DiaChi != user.DiaChi) {
      user.DiaChi = temp.DiaChi;
    }

    await tkModel.editUser(id, user);
    res.locals.authUser = user;
    req.session.authUser=user;
    res.render("account/profile", {
      layout: "account",
      mes: "Cập nhật thông tin thành công",
    });
    return;
  } else {
    res.render("/profile", {
      err: "Cập nhật thông tin không thành công",
    });
    return;
  }
});
export default router;
