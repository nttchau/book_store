import express from "express";
import taikhoanModel from "../models/taikhoan.model.js";
import bcrypt from "bcrypt";

const router = express.Router();

router.get("/", async (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  const userList = await taikhoanModel.findAll();
  res.render("management/user", { layout: "account.hbs", userList });
});

router.post("/delete", async (req, res) => {
  const delID = req.body.id;
  await taikhoanModel.delete(delID);
  res.redirect(req.headers.referer);
});

router.post("/unblock", async (req, res) => {
  const delID = req.body.id;
  await taikhoanModel.unblock(delID);
  res.redirect(req.headers.referer);
});

router.get("/add", (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  res.render("add/user", { layout: "account.hbs" });
});

router.post("/add", async (req, res) => {
  const rawPassword = req.body.password;
  if (rawPassword !== req.body.confirm) {
    res.render("add/user", {
      layout: "account.hbs",
      err_message: "Mật khẩu không khớp",
    });
    return;
  }
  const check = await taikhoanModel.findByEmail(req.body.Email);
  if (check !== null) {
    res.render("add/user", {
      layout: "account.hbs",
      err_message: "Tài khoản đã tồn tại",
    });
    return;
  }
  const address =
    req.body.street + ", " + req.body.district + ", " + req.body.city;
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(rawPassword, salt);
  const Loai = req.body.Loai;
  const user = {
    Email: req.body.Email,
    pssword: hash,
    HoTen: req.body.HoTen,
    DiaChi: address,
    SDT: req.body.SDT,
    Loai,
    HoatDong: true,
  };

  await taikhoanModel.add(user);
  res.render("add/user", {
    layout: "account.hbs",
    mess: "Tài khoản được tạo thành công!",
  });
  return;
});

router.get("/edit/:id", async (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "admin") {
    res.redirect("/");
    return;
  }
  const id = req.params.id || 0;

  const userInfo = await taikhoanModel.findById(id);
  res.render("edit/user", {
    layout: "account.hbs",
    userInfo,
  });
});

router.post("/edit/:id", async (req, res) => {
  const id = req.params.id || 0;
  const HoTen = req.body.HoTen;
  const SDT = req.body.SDT;
  const DiaChi = req.body.DiaChi;

  var user = await taikhoanModel.findById(id);

  if (user !== null) {
    if (HoTen === user.HoTen && SDT === user.SDT && DiaChi === user.DiaChi) {
      return;
    }

    if (HoTen != user.HoTen) {
      user.HoTen = HoTen;
    }
    if (SDT != user.SDT) {
      user.SDT = SDT;
    }
    if (DiaChi != user.DiaChi) {
      user.DiaChi = DiaChi;
    }

    await taikhoanModel.editUser(id, user);

    res.redirect(req.headers.referer);
  }
});

export default router;
