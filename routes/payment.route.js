import express from "express";
import auth from "../middlewares/auth.mdw.js";
import bookModel from "../models/book.model.js";
import mailModel from "../models/mail.model.js";
import orderDetailModel from "../models/order-detail.model.js";
import orderModel from "../models/order.model.js";
import numeral from "numeral";

const router = express.Router();

router.get("/purchase", auth, (req, res) => {
  console.log(req.session.cart);
  res.render("purchase", { layout: "main.hbs" });
});
router.post("/purchase", auth, async (req, res) => {
  const cart = req.session.cart;
  const info = req.body;
  let billDetail = "";
  let total = 0;
  for (let ci of cart) {
    let p = await bookModel.findPriceById(ci.id);
    total += p * ci.quantity;
  }
  //lưu đơn đặt hàng
  let TinhTrang = 1;
  if (info.payType == "1") {
    TinhTrang = 0;
  }
  if (info.transportType == "2") {
    total += 15000;
  }
  const dondathang = {
    TongTien: total,
    PTThanhToan: info.payType,
    PTGiaoHang: info.transportType,
    ThoiDiemDatHang: new Date(),
    NguoiDat: res.locals.authUser.id,
    TenNguoiNhan: info.recvName,
    SDTNguoiNhan: info.recvPhone,
    DiaChiNguoiNhan: info.recvAddress,
    TinhTrang,
  };
  await orderModel.add(dondathang);
  //lưu chi tiết đơn đặt hàng
  const order_id = await orderModel.getLastId();
  for (let ci of cart) {
    let item = {
      id: order_id,
      SachId: ci.id,
      SoLuong: ci.quantity,
    };
    await orderDetailModel.add(item);
    let before = await bookModel.getStock(ci.id);
    let after = before - ci.quantity;
    await bookModel.updateStock(ci.id, after);

    let bookTemp = await bookModel.findById(ci.id);
    billDetail += `\t${bookTemp.TenSach}
    \t- Đơn giá: ${numeral(bookTemp.GiaSach).format("0,0")} VNĐ
    \t- Số lượng: ${ci.quantity}\n`;
  }
  if (info.transportType == "2") {
    billDetail += `Giao hàng hỏa tốc: 15,000 VNĐ\n`;
  }
  billDetail += `Tổng tiền: ${numeral(total).format("0,0")} VNĐ`;
  //gửi mail
  mailModel.sendAuctionEmail(
    res.locals.authUser.Email,
    "[FAHAHA] Thông tin đơn hàng",
    `Người nhận: ${dondathang.TenNguoiNhan}
Số điện thoại: ${dondathang.SDTNguoiNhan}
Địa chỉ nhận hàng: ${dondathang.DiaChiNguoiNhan}
Chi tiết đơn hàng:
${billDetail}
    `
  );

  req.session.cart = [];

  //đưa về history
  res.redirect("/history");
});

router.get("/history", auth, async (req, res) => {
  let list;
  list = await orderModel.findUserOrder(req.session.authUser.id);
  console.log(list);
  res.render("account/history", { layout: "account.hbs", list });
});

router.get("/order/:id", auth, async (req, res) => {
  const orderId = req.params.id;
  const list = await orderDetailModel.findById(orderId);
  console.log(list);
  const method = await orderModel.getTransport(orderId);
  const TongTien = await orderModel.getTotal(orderId);
  console.log(TongTien);

  res.render("account/order", {
    layout: "account.hbs",
    list,
    TongTien,
    extra: method == 2,
  });
});
export default router;
