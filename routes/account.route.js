import express from "express";
import Recaptcha from "express-recaptcha";
import bcrypt from "bcrypt";
import auth from "../middlewares/auth.mdw.js";
const router = express.Router();
var recaptcha = new Recaptcha.RecaptchaV2(
  "6LeOKdsdAAAAAO1tRxxSCIoufTKvDLRmuC8Cq7BL",
  "6LeOKdsdAAAAACIZLMh8Osrrj5cJmsNDnC-TpsT1"
);
import tkModel from "../models/taikhoan.model.js";
router.get("/login", (req, res) => {
  if (res.locals.authUser) {
    res.redirect("/");
    return;
  }
  res.render("login", { layout: "authentication" });
});
router.post("/login", recaptcha.middleware.verify, async function (req, res) {
  if (!req.recaptcha.error) {
    const user = await tkModel.findByEmail(req.body.Email);
    if (user === null) {
      res.render("login", {
        layout: "authentication",
        err_message: "Email hoặc password không hợp lệ",
      });
      return;
    }
    const check = bcrypt.compareSync(req.body.password, user.pssword);
    if (check === false) {
      res.render("login", {
        layout: "authentication",
        err_message: "Email hoặc password không hợp lệ",
      });
      return;
    }
    req.session.auth = true;
    req.session.authUser = user;

    const url = "/";
    res.redirect(url);
  } else {
    res.render("login", {
      layout: "authentication",
      err_message: "Bạn bỏ sót reCaptcha",
    });
  }
});
router.get("/register", (req, res) => {
  if (res.locals.authUser) {
    res.redirect("/");
    return;
  }
  res.render("register", { layout: "authentication" });
});
router.post(
  "/register",
  recaptcha.middleware.verify,
  async function (req, res) {
    // console.log(req.body);

    if (!req.recaptcha.error) {
      const rawPassword = req.body.password;
      if (rawPassword !== req.body.confirm) {
        res.render("register", {
          layout: "authentication",
          err_message: "Mật khẩu không khớp",
        });
        return;
      }
      const check = await tkModel.findByEmail(req.body.Email);
      if (check !== null) {
        res.render("register", {
          layout: "authentication",
          err_message: "Tài khoản đã tồn tại",
        });
        return;
      }
      const address =
        req.body.street + " " + req.body.district + " " + req.body.city;
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(rawPassword, salt);
      const user = {
        Email: req.body.Email,
        pssword: hash,
        HoTen: req.body.HoTen,
        DiaChi: address,
        SDT: req.body.SDT,
        Loai: "user",
        HoatDong: true,
      };
      await tkModel.add(user);
      req.session.auth = true;
      req.session.authUser = user;
      res.redirect("/");
    } else {
      res.render("register", {
        layout: "authentication",
        err_message: "Bạn bỏ sót reCaptcha",
      });
    }
  }
);
router.get("/logout", async function (req, res) {
  req.session.auth = false;
  req.session.authUser = null;
  req.session.cart = [];
  const url = req.headers.referer || "/";
  res.redirect(url);
});


router.get("/checkPw", auth, (req, res) => {
  const pw = req.query.oldPw;
  return res.json(bcrypt.compareSync(pw, res.locals.authUser.pssword));
});
export default router;
