import express from "express";
import bookModel from "../models/book.model.js";
import categoryModel from "../models/category.model.js";

const router = express.Router();

router.get("/category/:id", async (req, res) => {
  const catID = req.params.id;

  const limit = 8;
  const page = req.query.page || 1;
  const offset = (page - 1) * limit;

  const total = await bookModel.countByCat(catID);
  let nPages = Math.floor(total / limit);
  if (total % limit > 0) {
    nPages++;
  }
  const pageNumbers = [];
  for (let i = 1; i <= nPages; i++) {
    pageNumbers.push({
      value: i,
      isCurrent: +page === i,
    });
  }
  let previous = +page - 1;
  let next = +page + 1;

  const list = await bookModel.findPageByCat(catID, limit, offset);

  const TheLoai = await categoryModel.findByCatId(catID);
  // console.log(TheLoai);
  TheLoai.sortTime = 1;
  res.render("category", {
    layout: "main.hbs",
    books: list,
    empty: list.length === 0,
    TheLoai,
    catId: catID,
    pageNumbers,
    can_go_prev: +page > 1,
    can_go_next: +page < nPages,
    previous,
    next,
  });
});

router.get("/category/sortPrice/:id", async (req, res) => {
  const catID = req.params.id;

  const limit = 8;
  const page = req.query.page || 1;
  const offset = (page - 1) * limit;

  const total = await bookModel.countByCat(catID);
  let nPages = Math.floor(total / limit);
  if (total % limit > 0) {
    nPages++;
  }
  const pageNumbers = [];
  for (let i = 1; i <= nPages; i++) {
    pageNumbers.push({
      value: i,
      isCurrent: +page === i,
    });
  }
  let previous = +page - 1;
  let next = +page + 1;

  const list = await bookModel.findPageByCatPrice(catID, limit, offset);

  const TheLoai = await categoryModel.findByCatId(catID);
  // console.log(TheLoai);
  TheLoai.sortPrice = 1;
  res.render("category", {
    layout: "main.hbs",
    books: list,
    empty: list.length === 0,
    TheLoai,
    catId: catID,
    pageNumbers,
    can_go_prev: +page > 1,
    can_go_next: +page < nPages,
    previous,
    next,
  });
});
router.get("/detail/:id", async (req, res) => {
  const bookID = req.params.id;

  const list = await bookModel.findById(bookID);
  if (list === null) {
    return res.redirect("/");
  }
  const catID = list.TheLoai;
  const TheLoai = await categoryModel.findByCatId(catID);
  res.render("detail", {
    layout: "main.hbs",
    book: list,
    cat: TheLoai.TenTheLoai,
    catId: catID,
  });
});
router.get("/search", async (req, res) => {
  const filterSearch = String(req.query.filterSearch || 0);
  const searchInput = String(req.query.searchInput || 0);
  const limit = 8;
  const page = req.query.page || 1;
  const offset = (page - 1) * limit;

  const total = await bookModel.countSearch(searchInput, filterSearch);
  // console.log(total);
  let nPages = Math.floor(total / limit);
  if (total % limit > 0) {
    nPages++;
  }
  const pageNumbers = [];
  for (let i = 1; i <= nPages; i++) {
    pageNumbers.push({
      value: i,
      isCurrent: +page === i,
    });
  }
  let previous = +page - 1;
  let next = +page + 1;

  const list = await bookModel.searchBooks(
    searchInput,
    filterSearch,
    limit,
    offset
  );

  let searchName = { search: "", cate: "" };
  searchName.search = searchInput;
  searchName.cate = filterSearch;
  searchName.sortTime = 1;
  // console.log(searchName);

  res.render("category", {
    layout: "main.hbs",
    books: list,
    empty: list.length === 0,
    keywork: String(searchInput),
    pageNumbers,
    searchName,
    can_go_prev: +page > 1,
    can_go_next: +page < nPages,
    previous,
    next,
  });
  return;
});
router.get("/search/sortPrice", async (req, res) => {
  const filterSearch = String(req.query.filterSearch || 0);
  const searchInput = String(req.query.searchInput || 0);
  const limit = 8;
  const page = req.query.page || 1;
  const offset = (page - 1) * limit;

  const total = await bookModel.countSearch(searchInput, filterSearch);
  // console.log(total);
  let nPages = Math.floor(total / limit);
  if (total % limit > 0) {
    nPages++;
  }
  const pageNumbers = [];
  for (let i = 1; i <= nPages; i++) {
    pageNumbers.push({
      value: i,
      isCurrent: +page === i,
    });
  }
  let previous = +page - 1;
  let next = +page + 1;

  const list = await bookModel.searchBooksPrice(
    searchInput,
    filterSearch,
    limit,
    offset
  );

  let searchName = { search: "", cate: "" };
  searchName.search = searchInput;
  searchName.cate = filterSearch;
  searchName.sortPrice = 1;
  // console.log(searchName);

  res.render("category", {
    layout: "main.hbs",
    books: list,
    empty: list.length === 0,
    keywork: String(searchInput),
    pageNumbers,
    searchName,
    can_go_prev: +page > 1,
    can_go_next: +page < nPages,
    previous,
    next,
  });
  return;
});
export default router;
