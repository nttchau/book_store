import express from "express";
import moment from "moment";
import cartModel from "../models/cart.model.js";
import bookModel from "../models/book.model.js";
const router = express.Router();

router.get("/", async (req, res) => {
  if (res.locals.auth === false) {
    res.redirect("/login");
    return;
  }
  if (res.locals.authUser.Loai != "user") {
    res.redirect("/");
    return;
  }

  const items = [];
  let totalPrice = 0;
  for (let cartElement of req.session.cart) {
    const book = await bookModel.findById(cartElement.id);
    totalPrice += book.GiaSach * cartElement.quantity;
    items.push({
      book,
      quantity: cartElement.quantity,
      amount: book.GiaSach * cartElement.quantity,
    });
  }

  // console.log(items);
  res.render("account/cart", {
    layout: "account.hbs",
    items,
    empty: items.length === 0,
    totalPrice,
  });
});

router.post("/add", async (req, res) => {
  if (res.locals.auth === false) {
    res.redirect("/login");
    return;
  }
  if (res.locals.authUser.Loai != "user") {
    res.redirect("/");
    return;
  }
  const item = {
    id: +req.body.id,
    quantity: +req.body.quantity,
  };
  cartModel.add(req.session.cart, item);
  // console.log(req.session.cart);
  res.redirect(req.headers.referer);
});

router.post("/update", async (req, res) => {
  if (res.locals.auth === false) {
    res.redirect("/login");
    return;
  }
  if (res.locals.authUser.Loai != "user") {
    res.redirect("/");
    return;
  }
  const item = {
    id: +req.body.id,
    quantity: +req.body.quantity,
  };
  cartModel.update(req.session.cart, item);
  // console.log(req.session.cart);
  res.redirect(req.headers.referer);
});

router.post("/del", async (req, res) => {
  if (res.locals.auth === false || res.locals.authUser.Loai != "user") {
    res.redirect("/");
    return;
  }
  cartModel.del(req.session.cart, +req.body.id);
  res.redirect(req.headers.referer);
});
router.post("/checkout", async (req, res) => {
  let total = 0;
  const details = [];
  for (let cartElement of req.session.cart) {
    const product = await productModel.findById(cartElement.id);
    const amount = product.Price * cartElement.quantity;
    total += amount;

    details.push({
      ProID: product.ProID,
      Quantity: cartElement.quantity,
      Price: product.Price,
      Amount: amount,
      OrderID: 0,
    });
  }

  const order = {
    OrderDate: moment().format("YYYY-MM-DD HH:mm:ss"),
    UserID: req.session.authUser.id,
    Total: total,
  };

  const ret = await orderModel.add(order);
  for (const detail of details) {
    detail.OrderID = ret[0];
    await detailModel.add(detail);
  }

  req.session.cart = [];
  res.redirect(req.headers.referer);
});

export default router;
