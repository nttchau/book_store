import express from "express";
import morgan from "morgan";


import viewMdw from "./middlewares/view.mdw.js";
import routesMdw from "./middlewares/routes.mdw.js";
import sessionMdw from "./middlewares/session.mdw.js";
import activate_locals_middleware from "./middlewares/locals.mdw.js";
const port = 3000;
const app = express();
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(morgan("dev"));

app.use(express.static("public/images"));
app.use(express.static("public/css"));


sessionMdw(app);
activate_locals_middleware(app);
viewMdw(app);
routesMdw(app);

app.listen(port, function () {
  console.log("http://localhost:3000/");
});
