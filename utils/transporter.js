import nodemailer from "nodemailer";

export const fromMail = process.env.EMAIL;

// e-mail transport configuration
export let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: fromMail,
    pass: process.env.EMAIL_PASSWORD,
  },
});
