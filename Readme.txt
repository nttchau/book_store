GuideLine How To Run App On Localhost

1. Run the SQL file.
2. Open the terminal on your system.
3. Navigate to the folder containing file "app.js".
4. If this is the first time you run this app on localhost server, run command "npm i" to install node_modules.
5. Create a .env file in the root of your project. 

DB_HOST="127.0.0.1"
DB_PORT=port
DB_USER=your_username
DB_PASSWORD=your_password
DB_DATABASE="bookstore"
EMAIL=your_email (this email auto send an email to user when they purchase their bills.)
EMAIL_PASSWORD=your_email_password

4. To open program in localhost, run command "node app.js"
5. Go to your browser and type "http://localhost:3000"



Username and password each type of account to demo:
- Admin:
    + Email: abc1@gmail.com
    + Password: abc
- Staff:
    + Email: abc2@gmail.com
    + Password: abc
- User:
    + Email: abc@gmail.com
    + Password: abc
